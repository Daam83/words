package com.daamapp.slowka;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView textView;
    //    private Activity myActivity;
    private String a;
    Words words = new Words();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        myActivity = this;


    }

    public void next(View view) {

        textView = (TextView) findViewById(R.id.textView);
        textView.setText(words.wordsSingle());
    }
}
